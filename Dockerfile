# Use my custom base image
FROM hencian/jmeter_redis:jmeterbaseh

MAINTAINER Horie

# Expose port for JMeter Master
EXPOSE 60000
